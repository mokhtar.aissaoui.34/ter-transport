package Graphe;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class test {

    public static void main(String[] args) {

        String content = "This is the content to write into file\n";

        try (FileWriter writer = new FileWriter("okokmekmek.txt");
             BufferedWriter bw = new BufferedWriter(writer)) {

            bw.write(content);

        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }

    }
}
