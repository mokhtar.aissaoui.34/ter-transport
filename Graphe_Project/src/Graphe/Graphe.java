package Graphe;


import java.util.*;
import java.lang.*;
import java.sql.*;
import java.text.*;
import java.io.*;
@SuppressWarnings({ "unchecked", "unused" })
public class Graphe {
	private static int nmax = 30;
	public ArrayList<ArrayList<Integer>> g;//matrice d'incidence
	public ArrayList<Sommet> v;//liste sommets correspondants (villes)
	public ArrayList<ArrayList<Couleur>> c;//matrice des couleurs correspondantes
	public boolean[] valid;//si un sommet doit etre visible, alors on le met valide
	
	public static Sommet sNull(){//pour le constructeur
		return new Sommet("sNull",0,0);//c'est notre convention pour un sommet nul
	}
	
	public Graphe(){
        g = new ArrayList<ArrayList<Integer>>();//voir attributs
        v = new ArrayList<Sommet>();
        c = new ArrayList<ArrayList<Couleur>>();
        ArrayList<Integer> aux = new ArrayList<Integer>();//pour remplir initialement la matrice d'incidence de tableaux nuls
        ArrayList<Couleur> aux2 = new ArrayList<Couleur>();//pareil pour la matrice des couleurs
        valid = new boolean[nmax];
        for(int i = 0; i < nmax; i++) {
        	aux.add(0);
        	aux2.add(Couleur.NULL);
        	valid[i] = false;//ainsi aucun sommet n'existe initialement
        	v.add(sNull());//on va remplir le tableau de sommets de sommets nuls initialement
        }//for i
        for(int j = 0; j < nmax; j++) {
        	g.add((ArrayList<Integer>) aux.clone());//on remplit la matrice d'incidence de copie de tableaux remplis de 0, ainsi on obtient une matrice de 0
        	c.add((ArrayList<Couleur>) aux2.clone());//pareil pour la matrice des couleurs
        }//for j
    }//constructeur par d�faut

	
	
	public Graphe(Graphe gr){
        g = (ArrayList<ArrayList<Integer>>) gr.getG().clone();//on clone tout simplement la matrice d'incidence du graphe en param�tres
        v = (ArrayList<Sommet>) gr.getV().clone();//pareil pour le tableau de sommets
        c = (ArrayList<ArrayList<Couleur>>) gr.getC().clone();//pareil pour la matrice des couleurs
        valid = new boolean[nmax];
        boolean[] v2 = gr.getValid();
        for(int i = 0; i<nmax; i++){
            valid[i]= v2[i];//on copie le tableau de validit�
        }//for i
    }//constructeur par copie
		
	public ArrayList<ArrayList<Integer>> getG() {
		return g;
	}


	public void setG(ArrayList<ArrayList<Integer>> g) {
		this.g = g;
	}

	public ArrayList<Sommet> getV() {
		return v;
	}


	public void setV(ArrayList<Sommet> v) {
		this.v = v;
	}
	
	public ArrayList<ArrayList<Couleur>> getC() {
		return c;
	}


	public void setC(ArrayList<ArrayList<Couleur>> c) {
		this.c = c;
	}
	
	public boolean[] getValid() {
		return valid;
	}


	public void setValid(boolean[] valid) {
		this.valid = valid;
	}
	
	public int getnmax(){
        return valid.length;//par def
    }
	
	public static void setnmax(int i){
		nmax = i;
	}
	
	public boolean existe(int i){
        return (i>=0 && i<getnmax());
    }
  
    public boolean existeSommet(int i){
        return(i>=0 && i<getnmax() && valid[i]);//un sommet existe s'il est valide et est entre 0 et nmax
    }
   
    public int nbSommet(){
        int count = 0;
        for(int i = 0; i<valid.length; i++){
            if(valid[i]){//on compte ici le nombre de true dans le tableau de validit�
                count++;
            }//if
        }//for i
        return count;
    }
  
    public boolean existeArc(int i, int j){//un arc existe si son origine et sa destination sont valides et si son poids est non-nul
        return (existeSommet(i) && existeSommet(j) && g.get(i).get(j)!=0);
    }
    

    public boolean estValide(int i){//true si le sommet d'indice i existe, erreur sinon
        if(existe(i)){
            return valid[i];
        }//if
        else{
            System.out.println("estValide: Argument invalide");
            System.exit(-1);
            return false;
        }//else
    }
  
    public int getDist(int i, int j){//renvoit le POIDS de l'arc de i � j, pas la dist /!\
        if(existeArc(i,j)){
            return g.get(i).get(j);
        }//if
        else{
            System.out.println("getValArc: Arguments invalides");
            System.exit(-1);
            return 0;//arbitraire
        }//else
    }
    
    public boolean ajoutSommet(int i){//true si le sommet a �t� cr��, false sinon
        if(i>=0 && i<getnmax()){
            if(valid[i]){
                return false;//si le sommet existe d�j�, renvoie false
            }//if
            else{
                valid[i] = true;//sinon cr�e le sommet            
                return true;//puis renvoie true
            }//else
        }//if
        else{
            System.out.println("ajoutSommet: Argument invalide");
            System.exit(-1);
            return false;
        }//else
    }
    
    public boolean ajoutSommetSkout(int i){//version silencieuse d'ajoutSommet, n'envoit pas de message d'erreur
        if(i>=0 && i<getnmax()){
            if(valid[i]){
                return false;//si le sommet existe d�j� , renvoie false
            }//if
            else{
                valid[i] = true;//sinon cr�e le sommet            
                return true;//puis renvoie true
            }//else
        }//if
        else{
            return false;
        }//else
    }
    
    public boolean supprimeSommet(int i){
        if(i>=0 && i<getnmax()){
            if(valid[i]){//si le sommet demand� existe bien:
                valid[i] = false;//supprime le sommet
                return true;//puis renvoie true
            }//if
            else{      
                return false;//sinon renvoie false
            }//else
        }//if
        else{
            System.out.println("supprimeSommet: Argument invalide");
            System.exit(-1);
            return false;
        }//else
    }
    
    public boolean bombeAtomique(String s){//efface completement le sommet
    	int i = this.indiceVille(s);
        if(i>=0 && i<getnmax()){
            if(valid[i]){//si le sommet demand� existe bien:
                for(int j = 0; j<getnmax(); j++){//supprime tous les arcs entrants et sortants de ce sommet
                	g.get(i).set(j,0);//les positions i,j et j,i sont mises � 0 (sym�trie)
                	g.get(j).set(i,0);
                }//for j
                valid[i] = false;//supprime le sommet
                return true;//puis renvoie true
            }//if
            else{      
                return false;//sinon renvoie false
            }//else
        }//if
        else{
            System.out.println("supprimeSommet: Argument invalide");
            System.exit(-1);
            return false;
        }//else
        
    }
    
    public boolean ajoutVille(String nom){//true si la ville a �t� ajout�e, false sinon
    	int i = 0;
    	while(i<nmax&&!ajoutSommetSkout(i)){//on va chercher le premier indice disponible pour rajouter un sommet
    		i++;//tant qu'on ne peut pas ajouter de sommet en i, on incr�mente
    	}//while i
    	if(i!=nmax){//si on est pas encore au max de la capacit�:
    		Sommet s = new Sommet();
    		String d=null;
    		s.setnom(nom);
    		try {
				Class.forName( "com.mysql.cj.jdbc.Driver" );
				Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ter?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "root", "terl3");
				Statement myStmt= myConn.createStatement() ;

				ResultSet myRs = myStmt.executeQuery("select * from villes");
					while (myRs.next()) 
							{
			
								if(myRs.getString("ville_nom_simple").matches(nom))
									{
										s.setNom_reel(myRs.getString("ville_nom_reel"));
										
										s.setLon( myRs.getDouble("ville_longitude_deg"));
										s.setLat( myRs.getDouble("ville_latitude_deg"))	;
									}//fin if 
										
							}//fin while
					myRs.close();
			}//fin try
		
			catch (Exception exc) 
			{
				exc.printStackTrace();
			}//fin catch
    		v.set(i, s);//on met notre sommet s construit au dessus � l'emplacement i, qui on l'a vu est le premier emplacement disponible
    		System.out.println("Sommet "+s.getNom_reel()+" ajout� en "+i +"((GPS ("+s.getLat()+" ;"+s.getLon()+" )");
    		return true;
    	}//if i
    	else{
			System.out.println("ajoutVille: Pas de place");
            System.exit(-1);
            return false;
		}//else i
    }
    
    public boolean ajoutArc(int i, int j, Couleur c){//true si on a bien ajout� l'arc, false sinon
    	if(existeSommet(i) && existeSommet(j)){
    		if(this.g.get(i).get(j)!=0) {
    			return false;//si l'arc existe d�j� , renvoie false
    		}//if
    		else{
    			int val=this.dist(v.get(i).getnom(),v.get(j).getnom());
    			this.g.get(i).set(j, val);//sinon cr�e l'arc
    			this.c.get(i).set(j, c);//et on ajoute la couleur
    			this.v.get(i).ajoutCouleur(c);
    			return true;//puis renvoie true
    		}//else
    	}//if
    	else{
    		System.out.println("ajoutArc: Argument invalide");
    		System.exit(-1);
    		return false;
    	}//else
    }
    
    public boolean supprimeArc(int i, int j){//true si on a bien supprim� l'arc, false sinon
    	if(existeSommet(i) && existeSommet(j)){
    		if(this.g.get(i).get(j)!=0){
    			this.g.get(i).set(j, 0);//si l'arc en question existe bien, supprime l'arc
    			return true;//puis renvoie true
    		}//if
    		else{
    			return false;//sinon renvoie false
    		}//else
    	}//if
    	else{
    		System.out.println("supprimeArc: Argument invalide");
    		System.exit(-1);
    		return false;
    		}//else
    	}
    
    /*public boolean estSymetrique(){
    	for(int i = 0; i<valid.length; i++){
    		for(int j = 0; j<valid.length; j++){
    			if(existeArc(i,j)&&!existeArc(j,i)){
    					return false;//si l'on trouve un couple de sommets {i,j} tel que l'arc (i,j) existe, mais pas l'arc (j,i), renvoie false
    			}//if
    		}//for j
    	}//for i
    	return true;//sinon renvoie true
    }
    
    public boolean estAntiSymetrique(){
    	for(int i = 0; i<valid.length; i++){
    		for(int j = 0; j<valid.length; j++){
    			if((i!=j)&&existeArc(i,j)&&existeArc(j,i)){
    					return false;//si l'on trouve un couple de sommets {i,j} tel que l'arc (i,j) existe, mais pas l'arc (j,i), renvoie false
    			}//if
    		}//for j
    	}//for i
    	return true;//sinon renvoie true
    }
   
    public boolean estTransitif(){
    	for(int i = 0; i<valid.length; i++){
    		for(int j = 0; j<valid.length; j++){
    			for(int k = 0; k<valid.length; k++){
    				if((i!=j)&&(j!=k)&&(existeArc(i,j)&&existeArc(j,k)&&!existeArc(i,k))){
    					return false;//si l'on trouve trois sommets {i,j,k} tels que les arcs (i,j) et (j,k) existent, mais pas (i,k) renvoie false
    				}//if
    			}//for k
    		}//for j
    	}//for i
    	return true;//sinon renvoie true
    }

    public boolean estAntiTransitif(){
    	for(int i = 0; i<valid.length; i++){
    		for(int j = 0; j<valid.length; j++){
    			for(int k = 0; k<valid.length; k++){
    				if((i!=j)&&(j!=k)&&(existeArc(i,j)&&existeArc(j,k)&&existeArc(i,k))){
    					return false;//si l'on trouve trois sommets {i,j,k} tels que les arcs (i,j), (j,k) et (i,k) existent, renvoie false
    				}//if
    			}//for k
    		}//for j
    	}//for i
    	return true;//sinon renvoie true
    }
    
    public void transposition(){
    	Graphe gr = new Graphe(this);//on copie le graphe
    	for(int i = 0; i<nmax; i++){
    		for(int j = 0; j<nmax; j++){
    			this.g.get(j).set(i,gr.g.get(i).get(j));//on transpose g.u dans this.u
    		}//for j
    	}//for i
    }

    public void union(Graphe gr){
    	if(valid.length!=gr.valid.length) {
    		System.out.println("union: Argument invalide (nb sommets)");
    		System.exit(-1);
    	}//if valid
    	for(int i = 0; i<valid.length; i++){
    		if(valid[i]!=gr.valid[i]) {
    			System.out.println("union: Argument invalide (sommets non existants)");
    			System.exit(-1);
    		}//if i
    	}//for i
    	for(int j = 0; j<valid.length; j++){
    		for(int k = 0; k<valid.length; k++){
    			if(gr.existeArc(j,k)) {
    				if(!existeArc(j,k)) {//par def
    					this.g.get(j).set(k,gr.g.get(j).get(k));
    				}//if !existe
    				else {
    					System.out.println("union: Argument invalide (arcs dupliqués)");
    					System.exit(-1);
    				}//else !existe
    			}//if gr.existe
    		}//for k
    	}//for j
    }
    */
    
    public void sousgraphe(int[] listeSommet){//c'est la liste des sommets que l'on veut supprimer
    	for(int i = 0; i<listeSommet.length; i++){
    		if(existeSommet(listeSommet[i])){
    			supprimeSommet(listeSommet[i]);
    		}//if
    		else{
    			System.out.println("sousgraphe: Argument invalide (sommets non existants)");
    			System.exit(-1);
    		}//else
    	}//for i
    }

    public void reinit(){//supprime tous les arcs sans pour autant supprimer de sommets
    	for(int i = 0; i<valid.length; i++){
    		for(int j = 0; j<valid.length; j++){
    			this.g.get(i).set(j,0);//on set tout � 0
    		}//for j
    	}//for i
    }
    
   
    
    //NOUVELLES METHODES
    
    public int indiceVille(String nom){//renvoit un indice � partir du nom du sommet
    	boolean b = false;
    	int i = 0;
    	while(i<nmax&&!b) {
    		b = (this.v.get(i).getnom().matches(nom));//comparaison de string
    		i++;
    	}//while
    	if(b){
    		i--;
    		//System.out.println(nom+" : "+i);
    		return i;
    	}//if
    	else{
    		System.out.println("indiceVille: Argument invalide");
			System.exit(-1);
    		return -1;
    	}//else
    	
    }
    
    public Sommet sommetVille(String nom){
    	return this.v.get(this.indiceVille(nom));//renvoit directement un sommet � partir d'un nom en param�tre
    }
    
    public ArrayList<Couleur> lignesPartantes(String nom){//renvoit la liste des couleurs partant du sommet
    	int i = this.indiceVille(nom);
    	if(i!=-1){
    		ArrayList<Couleur> c = this.v.get(i).clist;
    		/*if(c.size()!=0){
    			System.out.println(nom+" : "+c.toString());
    		}//if
    		else{
    			System.out.println("Aucune ligne ne part de "+nom);
    		}//else*/
    		return c;
    	}//if
    	else{
    		return new ArrayList<Couleur>();
    	}//else
    }
    
    public String voisinDirectLigne(String nom, Couleur c){//renvoit le successeur direct du sommet, par la ligne de couleur c
    	int i = 0;
    	int x = this.indiceVille(nom);
    	if(this.v.get(x).clist.contains(c)){//on teste si la couleur c part bien du sommet
    		boolean b = false;
    		while(i<nmax&&!b){
    			b = (this.c.get(x).get(i)==c);//true si d'apr�s la matrice de couleur on peut aller de x � i en utilisant la couleur c 
    			i++;
			}//while
    		i--;
    		return this.v.get(i).getnom();//on return le nom du sommet successeur
		}//if
    	else{
    		//System.out.println("voisinDirectLigne: La ligne "+c.toString()+" ne part pas de "+nom);
    		//System.exit(-1);
    		return "";
    	}//else
    }
    
    public ArrayList<String> voisinsDirect(String nom){//renvoit TOUS les sommets successeurs, par TOUTES les lignes qui partent du sommet en param�tre
    	ArrayList<String> s = new ArrayList<String>();
    	ArrayList<Couleur> c = this.lignesPartantes(nom);//la liste des couleurs partant du sommet
    	for(Couleur co : c){//on traite chaque couleur co de la liste c
    		String vois = this.voisinDirectLigne(nom, co);
    		if(!s.contains(vois)) {//si le voisin n'a pas d�j� �t� compt�
    			s.add(vois);//on l'ajoute � notre liste de ville
    		}//if
    	}//for co
    	return s;
    }
    
    
    
    //METHODES IMPORTANTES
    public ArrayList<String> villesAccMemesLignes(String nom){//retourne la liste des noms de villes accessibles en prenant UNE SEULE ligne (pas de changements)
    	ArrayList<Couleur> c = this.lignesPartantes(nom);//la liste des couleurs partant du sommet
    	ArrayList<String> liste = new ArrayList<String>();
    	//int x = this.indiceVille(nom);
    	for(Couleur co : c){//on traite chaque couleur co de la liste c
    		String aux = nom;
    		while(this.voisinDirectLigne(aux, co)!=""){
    			aux = this.voisinDirectLigne(aux, co);
    			if(!liste.contains(sommetVille(aux).getnom())){//si le voisin n'a pas d�j� �t� compt�
    				liste.add(sommetVille(aux).getnom());//on l'ajoute � notre liste de ville
    			}//if
    		}//while
    	}//for co
    	return liste;
    }
    
    public ArrayList<Trajet> villesAccMemesLignesTraj(String nom){//retourne la liste des trajets possibles en prenant UNE SEULE ligne (pas de changements)
    	ArrayList<Couleur> c = this.lignesPartantes(nom);//la liste des couleurs partant du sommet
    	ArrayList<Trajet> liste = new ArrayList<Trajet>();
    	//int x = this.indiceVille(nom);
    	for(Couleur co : c){//on traite chaque couleur co de la liste c
    		String aux = nom;
    		Trajet traj = new Trajet();
    		traj.ajoutDepart(this.sommetVille(nom));//le d�part du trajet est le sommet en param�tre
    		while(this.voisinDirectLigne(aux, co)!=""){//on suit la ligne de couleur co jusqu'au bout
    			aux = this.voisinDirectLigne(aux, co);
    			if(!traj.contains(aux)) {
    				traj.ajoutEtape(this.sommetVille(aux), co);
    				liste.add(new Trajet(traj));//on stocke une copie de traj, pour pouvoir continuer � le modifier
    			}//if
    		}//while
    	}//for co
    	return liste;
    }
    
    //utilis� pour villesAccTraj, la plus importante
    //m�me code que vamlt
    public ArrayList<Trajet> vamltCoul(String nom, ArrayList<Couleur> dejaPris){//villesAccMemesLignesTraj avec en plus une liste de couleurs � ne pas emprunter
    	ArrayList<Couleur> c = this.lignesPartantes(nom);
    	for(Couleur pris : dejaPris) {
    		c.remove(pris);
    	}//for pris
    	ArrayList<Trajet> liste = new ArrayList<Trajet>();
    	for(Couleur co : c){
    		String aux = nom;
    		Trajet traj = new Trajet();
    		traj.ajoutDepart(this.sommetVille(nom));
    		while(this.voisinDirectLigne(aux, co)!=""){
    			aux = this.voisinDirectLigne(aux, co);
    			if(!traj.contains(aux)) {
    				traj.ajoutEtape(this.sommetVille(aux), co);
    				liste.add(new Trajet(traj));
    			}//if
    		}//while
    	}//for co
    	return liste;
    }
    
    public ArrayList<String> villesAcc(String nom){//renvoit la liste de TOUTES les villes accessibles (autant de changement que l'on veut)
    	ArrayList<String> liste = this.villesAccMemesLignes(nom);
    	int i = 0;    	
    	while(i<liste.size()) {
    		ArrayList<String> temp = this.villesAccMemesLignes(liste.get(i));//on va simplement it�rer vaml et stocker les noms de ville dans liste
    		for(String ville : temp) {
    			if(!liste.contains(ville)&&!ville.matches(nom)){//�vite les boucles: on ne peut pas avoir le nom de la ville de d�part dans liste
    				liste.add(ville);
    			}//if
    		}//for ville
    		i++;
    	}//while
    	return liste;
    }
 
    //IMPORTANT
    //m�thode na�ve pour le projet
    public ArrayList<Trajet> villesAccTraj(String nom){//renvoit TOUS les trajets possibles (autant de changements que l'on veut)
    	ArrayList<Trajet> liste = this.villesAccMemesLignesTraj(nom);//on commence par se donner tous les trajets sans changements de lignes
    	ArrayList<Trajet> res = new ArrayList<Trajet>();//c'est la liste r�sultat
    	ArrayList<Trajet> temp = new ArrayList<Trajet>();
    	int i = 0;
    	while(i<liste.size()){//pour chaque trajet de la liste initiale
    		Trajet current = liste.get(i);
    		res.add(current);
    		Sommet etape = current.getDestination();//on prend la derni�re �tape
    		temp = vamltCoul(etape.getnom(),current.dejaPris());//on prend tous les trajets partant de l'�tape
    		for(Trajet traj : temp) {//pour chacun de ces trajets traj dans temp
    			res.add(current.ajoutTrajetNew(traj));//on ajoute � la liste r�sultat un nouveau trajet qui est la concat�nation de current et traj
    		}//for traj
    		i++;
    	}//while
    	return res;
    }
    
    public static String nom_reel(String a) {
    	String D=null;
    	try {
			Class.forName( "com.mysql.cj.jdbc.Driver" );
			Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ter?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "root", "terl3");
			Statement myStmt= myConn.createStatement() ;

			ResultSet myRs = myStmt.executeQuery("select * from villes");
				while (myRs.next()) 
						{
		
							if(myRs.getString("ville_nom_simple").matches(a))
								{
									D=myRs.getString("ville_nom_reel");
								}
						} //fin while
				myRs.close();
		}//fin try
	
		catch (Exception exc) 
		{
			exc.printStackTrace();
		}//fin catch
    	return D;
    }
  //distance entre deux points (villes)
    public int dist(String a,String b)
    {
		/*double	 x =0 ,
				 y = 0,
				 z=0,
				 j=0;
		Boolean f=false,
				t=false;
		String  D = null,
				A = null;
		try {
				Class.forName( "com.mysql.cj.jdbc.Driver" );
				Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ter?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "root", "terl3");
				Statement myStmt= myConn.createStatement() ;

				ResultSet myRs = myStmt.executeQuery("select * from villes");
					while (myRs.next()) 
							{
			
								if(myRs.getString("ville_nom_simple").matches(a))
									{
										D=myRs.getString("ville_nom_reel");
										f=true;
										x= myRs.getDouble("ville_longitude_deg");
										y= myRs.getDouble("ville_latitude_deg")	;
									}//fin if 
										else 
											{
											if(myRs.getString("ville_nom_simple").matches(b)) 
												{ 
													t=true;
													A =myRs.getString("ville_nom_reel");
													j=	myRs.getDouble("ville_longitude_deg");
													z= myRs.getDouble("ville_latitude_deg");
												}//fin if2
											}//fin else	
							}//fin while
					myRs.close();
			}//fin try
		
			catch (Exception exc) 
			{
				exc.printStackTrace();
			}//fin catch*/
		
		
		double R=6371;
		double latAR=Math.toRadians(this.sommetVille(a).getLat());
		double latBR=Math.toRadians(this.sommetVille(b).getLat());
		double lonAR=Math.toRadians(this.sommetVille(a).getLon());		
		double lonBR=Math.toRadians(this.sommetVille(b).getLon());
		//formule de calcul :
		double x;
		x=R*Math.acos(Math.sin(latAR)*Math.sin(latBR)+Math.cos(latAR)*Math.cos(latBR)*Math.cos(lonAR-lonBR));
		

			int d =(int) (x*120/100);
			
			
				return d ;
		
			
    }
    

    public ArrayList<Trajet> Trajetdispo(String D,String A){ //trouver les trajet disponible de D � A
    	ArrayList<Trajet> aux = new ArrayList<Trajet>();
    	ArrayList<Trajet> aux2 = this.villesAccTraj(D);
    	System.out.println(aux2);
    	for(int i=0;i<aux2.size();i++) {
    		System.out.println(aux2.get(i).getDestination().getnom());
    		if(A==aux2.get(i).getDestination().getnom()) {
    			
    			aux.add(aux2.get(i));
    		}
    	}
    	if(aux.size()==0) {
    		System.out.println("Il existe aucune proposition pour le trajet demand�");
    	}
		return aux;
    	
    }
    
    
    
    public static int distInd(Graphe gr, int a,int b) {
		double R=6371;
		double latAR=Math.toRadians(gr.v.get(a).getLat());
		double latBR=Math.toRadians(gr.v.get(b).getLat());
		double lonAR=Math.toRadians(gr.v.get(a).getLon());		
		double lonBR=Math.toRadians(gr.v.get(b).getLon());
		//formule de calcul :
		double x = R*Math.acos(Math.sin(latAR)*Math.sin(latBR)+Math.cos(latAR)*Math.cos(latBR)*Math.cos(lonAR-lonBR));
		int d =(int) (x*120/100);
		return d ;
    }
		
    public boolean exportG(String nom) throws IOException{
    	String exp = "";
    	
    	exp+="nmax\n";
    	exp+=nmax+"\n";
    	
    	exp+="\ng\n";
    	for(int i = 0; i<nmax; i++){
    		exp+=this.g.get(i).toString()+"\n";
    	}//for
    	
    	exp+="\nv\n";
    	for(int i = 0; i<nmax; i++){
    		exp+=this.v.get(i).toString()+"\n";
    	}//for
    	
    	exp+="\nc\n";
    	for(int i = 0; i<nmax; i++){
    		exp+=this.c.get(i).toString()+"\n";
    	}//for
    	
    	exp+="\nvalid\n[";
    	for(int i = 0; i<valid.length-1; i++){
    		exp+=this.valid[i]+", ";
    	}//for
    	exp+=this.valid[valid.length-1]+"]";
    	
        try (FileWriter writer = new FileWriter(nom+".txt");
             BufferedWriter bw = new BufferedWriter(writer)) {

            bw.write(exp);

        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
            return false;
        }//catch
        System.out.println(nom+" export� avec succ�s");
        return true;
    }
    
   public static Graphe importG(String nom) throws IOException{
    	
    	try (FileReader reader = new FileReader("D:\\Java\\Graphe Project\\"+nom+".txt");
    			BufferedReader br = new BufferedReader(reader)) {
    		Graphe temp = new Graphe();
    		String strLine = "";
    		
    		br.readLine();//nmax
    		int nm = Integer.parseInt(br.readLine());
    		String[] strT = new String[nm];
    		setnmax(nm);
    		
    		br.readLine();//saut de ligne
    		br.readLine();//g
    		for(int i = 0; i<nm; i++) {
    			strLine = br.readLine();
    			strLine = strLine.substring(1, strLine.length()-1);//j'enleve les crochets
    			strT = strLine.split(", ");
    			for(int j = 0; j<nm; j++) {
    				temp.g.get(i).set(j, Integer.parseInt(strT[j]));
    			}//for j
    		}//for i
    	
    		br.readLine();
    		br.readLine();//v
    		for(int i = 0; i<nm; i++) {
    			strLine = br.readLine();
    			strLine = strLine.substring(1, strLine.length()-1);//j'enleve les crochets
    			strT = strLine.split(",  ");
    			if(strT[0]=="sNull") {//[sNull,  0.0,  0.0,  []]
    				temp.v.add(sNull());
    			}//if
    			else {
    				Sommet ville = new Sommet(strT[0],Double.parseDouble(strT[1]),Double.parseDouble(strT[2]));
    				String substr = strT[3].substring(1,strT[3].length()-1);
    				if(substr.length()!=0){
    					for(String c : substr.split(", ")) {
    						ville.clist.add(Couleur.valueOf(c));
    					}//for c
    				}//if
    				temp.v.set(i, ville);    				
    			}//else
    		}//for i
    		
    		br.readLine();
    		br.readLine();//c
    		//System.out.println(br.readLine());
    		for(int i = 0; i<nm; i++) {
    			strLine = br.readLine();
    			//System.out.println(strLine);
    			strLine = strLine.substring(1, strLine.length()-1);//j'enleve les crochets
    			strT = strLine.split(", ");
    			for(int j = 0; j<nm; j++) {
    				temp.c.get(i).set(j, Couleur.valueOf(strT[j]));
    			}//for j
    		}//for i
    		
    		br.readLine();
    		br.readLine();//valid
    		strLine = br.readLine();
			strLine = strLine.substring(1, strLine.length()-1);//j'enleve les crochets
			strT = strLine.split(", ");
    		for(int i = 0; i<nm; i++) {
    			temp.valid[i] = Boolean.valueOf(strT[i]);
    		}//for i
    		
    		System.out.println(nom+" import� avec succ�s");
    		return temp;

           } catch (IOException e) {
               System.err.format("IOException: %s%n", e);
               return new Graphe();
           }//catch
    	
    }
    
    public static void traitement1(Graphe gr) {//prend la base de donn�e, juste des sommets avec des coordonn�es, pas d'arcs
    	for(int i = 0; i < gr.v.size(); i++) {
    		for(int j = 0; j < gr.v.size(); j++) {
    			gr.g.get(i).set(j, distInd(gr, i, j));
    		}//for j
    	}//for i
    }//on a alors ajout� TOUS les arcs, dans les deux sens
    
    public static int minTabInd(ArrayList<Integer> tab) {
    	int min = Integer.MAX_VALUE;
    	int ind = -1;
    	for(int i = 0; i<tab.size(); i++) {
    		if(tab.get(i)<min&&tab.get(i)>0) {
    			min = tab.get(i);
    			ind = i;
    		}//if
    	}//for
    	return ind;
    }
    
    public static void traitement2(Graphe gr, int nbarc) {
    	ArrayList<ArrayList<Integer>> temp = (ArrayList<ArrayList<Integer>>) (new Graphe()).g.clone();//on prend une matrice vide bien formee
    	for(int i = 0; i<gr.g.size(); i++) {
    		for(int iter = 0; iter<nbarc; iter++){
    			int ind = minTabInd(gr.g.get(i));
    			//System.out.println(i+" , "+ind+" , "+gr.g.get(i).get(ind));
    			temp.get(i).set(ind, gr.g.get(i).get(ind));
    			temp.get(ind).set(i, gr.g.get(i).get(ind));
    			gr.g.get(i).set(ind, 0);
    		}//for iter    		
    	}//for i 
    	//System.out.println(temp.toString());
    	gr.setG(temp);
    }
    
    public ArrayList<String> voisins(String nom){
    	ArrayList<String> vois = new ArrayList<String>();
    	int ind = this.indiceVille(nom);
    	ArrayList<Integer> liste = this.g.get(ind);
    	for(int i = 0; i<liste.size(); i++) {
    		if(liste.get(i)!=0) {
    			vois.add(this.v.get(i).getnom());
    		}//if
    	}//for i
    	return vois;
    }
    
    public static void traitement3(Graphe gr, int nbcoul) {
    	ArrayList<Couleur> liste = new ArrayList<Couleur>();
    	while(liste.size()<nbcoul){
    		Couleur temp = Couleur.random();
    		if(!liste.contains(temp)){
    			liste.add(temp);
    		}//if
    	}//while
    	for(int i = 0; i<nbcoul; i++) {
    		int ind = ((int) Math.floor(gr.nbSommet()*Math.random()));
    		Sommet current = gr.v.get(ind);
    		current.clist.add(liste.get(i));
    		int lon = ((int) Math.floor(2*Math.random())+2);
    		for(int j = 0; j<lon; j++) {
    			ArrayList<String> vois = gr.voisins(current.getnom());
    			int rand = ((int) Math.floor(vois.size()*Math.random()));
    			gr.c.get(ind).set(rand, liste.get(i));
    			current = gr.v.get(rand);
    		}//for j
    	}//for i
    	
    }
    
    
    
    
    public static void main(String[] args) throws IOException{
    	//Graphe test = new Graphe();
    	
    	/*
    	try {
			Class.forName( "com.mysql.cj.jdbc.Driver" );
			Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ter?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "root", "terl3");
			Statement myStmt= myConn.createStatement() ;
			int i=0;
			ResultSet myRs = myStmt.executeQuery("select * from villes");
			while (myRs.next()&&i<200)
			{
				i++;
				test.ajoutVille(myRs.getString("ville_nom_simple"));
			}
			myRs.close();
		}//fin try
	
		catch (Exception exc) 
		{
			exc.printStackTrace();
		}//fin catch
    	test.exportG("okokmek");*/
    	/*
   
    	test.ajoutVille("montpellier");//0
    	test.ajoutVille("nimes");
    	test.ajoutVille("lunel");
    	test.ajoutVille("sete");
    	test.ajoutVille("perpignan");
    	test.ajoutVille("arles");//5
    	test.ajoutVille("aubenas");
    	test.ajoutVille("toulouse");
    	test.ajoutVille("bordeaux");
    	test.ajoutVille("marseille");
    	test.ajoutVille("pau");//10
    	//System.out.println(test.indiceVille("Montpellier"));
    	test.ajoutArc(4, 3, Couleur.ROUGE);
    	test.ajoutArc(3, 0, Couleur.ROUGE);
    	test.ajoutArc(0, 2, Couleur.ROUGE);
    	
    	//ligne bleue
    	test.ajoutArc(8, 7, Couleur.BLEU);
    	test.ajoutArc(7, 4, Couleur.BLEU);
    	test.ajoutArc(4, 10, Couleur.BLEU);
    	//ligne verte
    	test.ajoutArc(7, 0, Couleur.VERT);
    	test.ajoutArc(0, 1,Couleur.VERT);
    	test.ajoutArc(1, 6, Couleur.VERT);
    	
    	//ligne jaune
    	test.ajoutArc(1, 5, Couleur.JAUNE);
    	//System.out.println(test.getDist(1, 5));
    	test.ajoutArc(5, 9, Couleur.JAUNE);
    	
    	System.out.print("\n");
    	//test.indiceVille("Toulouse");
    	//test.lignesPartantes("Toulouse");
    	//test.lignesPartantes("Aubenas");
    	
    	//test.villesAccMemesLignes("Toulouse");
    	//System.out.println(test.voisinDirectLigne("Toulouse", Couleur.BLEU));
    	//System.out.println(test.voisinDirectLigne("Toulouse", Couleur.ROUGE));
    	
    	System.out.println(test.villesAccMemesLignes("toulouse"));*/
    	
    	/*
    	System.out.print("\n");
    	Trajet traj = new Trajet();
    	traj.ajoutDepart(test.sommetVille("Bordeaux"));
    	traj.ajoutEtape(test.sommetVille("Toulouse"),Couleur.BLEU);
    	traj.ajoutEtape(test.sommetVille("Montpellier"),Couleur.VERT);
    	traj.ajoutEtape(test.sommetVille("N�mes"),Couleur.VERT);
    	traj.ajoutEtape(test.sommetVille("Arles"),Couleur.JAUNE);
    	traj.ajoutEtape(test.sommetVille("Marseille"),Couleur.JAUNE);
    	
    	System.out.println(traj.toString());*/
    	/*
    	System.out.println(test.villesAccMemesLignesTraj("toulouse"));
    	System.out.println(test.villesAcc("toulouse"));
    	System.out.print("\n");
    	System.out.print("\n");
    	System.out.println(test.villesAccTraj("toulouse"));*/
    	/*
    	System.out.print("\n");

    	//System.out.println(test.voisinsDirect("Montpellier"));
    	System.out.println(test.dist("montpellier","toulouse"));
    	test.exportG("okokokmek2");*/
    	//Graphe testreader = importG("okokokmek2");
    	//System.out.println(testreader.v.get(6).getnom());
    	//testreader.exportG("testtttt");
    	
    	//Trajet trajtest = new Trajet();
    	//trajtest.ajoutDepart(test.sommetVille("toulouse"));
    	//trajtest.ajoutEtape(test.sommetVille("montpellier"), Couleur.VERT);
    	//System.out.println(test.vamltCoul("montpellier",trajtest.dejaPris()));
    	/*
    	Graphe enCours = importG("original30");
    	traitement1(enCours);
    	enCours.exportG("traitement1");
    	*/
    	/*
    	Graphe enCours = importG("traitement1");
    	traitement2(enCours,4);
    	enCours.exportG("traitement2_n4");*/
    	/*
    	Graphe enCours = importG("traitement2_n4");
    	traitement3(enCours,10);*/
    	
	}
}
    