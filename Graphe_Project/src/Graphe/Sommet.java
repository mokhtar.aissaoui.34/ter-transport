package Graphe;
import java.util.*;

public class Sommet {
	
	 //public int num;//numero
	 //public int att;//attente
	 public ArrayList<Couleur> clist = new ArrayList<Couleur>();//liste de couleurs
	 public String nom;
	 public String nom_reel;
	 public double lon;
	 public double lat;
	 
	 public Sommet(){
		 //num = 0;
		 //att = 0;
		 nom = "";
		 nom_reel="";
		 lon = 0;
		 lat = 0;
		 
	 }//constructeur par défaut (sans arguments)
	 
	

	public Sommet(String s, double lon, double lat){
	        //num = n;
	        //att = a;
	        nom = s;
	        this.lat=lat;
	        this.lon=lon;
	        nom_reel="";
	        
	    }//constructeur avec arguments
	 
	   /*public int getnum(){
	        return num;
	    }*/
	   /*
	    public Integer getatt(){
	        return att;
	    }*/
	    
	    public String getnom(){
	        return nom;
	    }
	   
	   /* public void setnum(int n){
	        num = n;
	    }*/
	   /*
	    public void setatt(Integer a){
	        att = a;
	    }*/
	    
	    public void setnom(String n){
	        nom = n;
	    }
	    
	    public double getLon() {
			return lon;
		}

		public void setLon(double lon) {
			this.lon = lon;
		}

		public double getLat() {
			return lat;
		}

		public void setLat(double lat) {
			this.lat = lat;
		}
	    
	    public boolean ajoutCouleur(Couleur c){
	    	if(!this.clist.contains(c)){
	    		clist.add(c);
	    		return true;
	    	}//if
	    	else{
	    		//System.out.println("ajoutCouleur: Argument répété");
	    		//System.exit(-1);
	    		return false;
	    	}//else
	    }
	    
	    
	    public String getNom_reel() {
			return nom_reel;
		}



		public void setNom_reel(String nom_reel) {
			this.nom_reel = nom_reel;
		}



		public String toString() {
	    	String s = "["+this.nom+",  "+this.lon+",  "+this.lat+",  "+this.clist.toString()+"]";
	    	return s;
	    }

}
