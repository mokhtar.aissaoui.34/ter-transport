package Graphe;

import java.util.*;
@SuppressWarnings({"unchecked"})
public class Trajet {
	public ArrayList<Sommet> etapes;
	public ArrayList<Couleur> couleurs;
	public int nb;
	
	public Trajet(){
		etapes = new ArrayList<Sommet>();
		couleurs = new ArrayList<Couleur>();
		nb = 0;
	}//constructeur par d�faut
	
	public Trajet(Trajet t){
		etapes = (ArrayList<Sommet>) t.getEtapes().clone();
		couleurs = (ArrayList<Couleur>) t.getCouleurs().clone();
		nb = t.getNb();
	}//constructeur par copie
	
	public ArrayList<Sommet> getEtapes(){
		return etapes;
	}
	public void setEtapes(ArrayList<Sommet> etapes){
		this.etapes = etapes;
	}
	public ArrayList<Couleur> getCouleurs(){
		return couleurs;
	}
	public void setCouleurs(ArrayList<Couleur> couleurs){
		this.couleurs = couleurs;
	}

	public int getNb(){
		return nb;
	}

	public void setNb(int nb){
		this.nb = nb;
	}
	
	public boolean ajoutDepart(Sommet s){
		if(nb==0&&etapes.size()==0){
			etapes.add(s);
			return true;
		}//if
		else{
			System.out.println("ajoutDepart: Argument invalide");
            System.exit(-1);
			return false;
		}//else
	}
	
	public boolean ajoutEtape(Sommet s, Couleur c){
		if(etapes.get(nb).clist.contains(c)){
			etapes.add(s);
			couleurs.add(c);
			nb++;
			return true;
		}//if
		else{
			//System.out.println("ajoutEtape: Argument invalide");
            //System.exit(-1);
			return false;
		}//else
	}
	
	public boolean ajoutTrajet(Trajet t, Couleur c){
		if(this.ajoutEtape(t.etapes.get(0), c)){
			for(int i = 1; i<=t.nb; i++){
				this.ajoutEtape(t.etapes.get(i), t.couleurs.get(i-1));
			}//for
			return true;
		}//if
		else{
			//System.out.println("ajoutTraj: Argument invalide");
            //System.exit(-1);
			return false;
		}//else
	}
	
	public Trajet ajoutTrajetNew(Trajet t){
		Trajet t2 = new Trajet(this);
		t2.ajoutTrajet(t, t.couleurs.get(0));
		return t2;
	}
	
	public Sommet getDestination() {
		return this.etapes.get(nb);
	}
	
	public ArrayList<Couleur> dejaPris(){
		ArrayList<Couleur> dP = new ArrayList<Couleur>();
		for(Couleur c : this.couleurs){
			if(!dP.contains(c)){
				dP.add(c);
			}//if
		}//for c
		return dP;
	}
	
	public boolean contains(String ville) {
		boolean b = false;
		int i = 0;
		while(!b&&i<=nb) {
			b = this.etapes.get(i).getnom().matches(ville);
			i++;
		}//while
		return b;
	}
	
	public String toString(){
		if(this.nb>0){
			String s = "\n"+"Ligne "+couleurs.get(0).toString()+": "+etapes.get(0).getNom_reel()+" -> "+etapes.get(1).getNom_reel();
			Couleur aux = couleurs.get(0);
			for(int i = 2; i<=this.nb; i++){
				if(couleurs.get(i-1)==aux){
					s += " -> "+etapes.get(i).getNom_reel();
				}//if
				else{
					aux = couleurs.get(i-1);
					s += " [CHANGEMENT] "+"Ligne "+couleurs.get(i-1).toString()+": "+/*etapes.get(i-1).getNom_reel()+" -> "+*/etapes.get(i).getNom_reel();
				}//else
			}//for i
			return s;
		}//if
		else{
			return "Trajet vide";
		}//else
	}
	
}
