package Graphe;
/* Simple graph drawing class
Bert Huang
COMS 3137 Data Structures and Algorithms, Spring 2009

This class is really elementary, but lets you draw 
reasonably nice graphs/trees/diagrams. Feel free to 
improve upon it!
 */

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class interfgr extends JFrame {
	   int width;
	    int height;

	    ArrayList<Node> nodes;
	    ArrayList<edge> edges;

	    public interfgr() {
	    	setBackground(SystemColor.activeCaptionBorder);
	    	getContentPane().setForeground(SystemColor.activeCaptionBorder); //Constructor
	        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		nodes = new ArrayList<Node>();
		edges = new ArrayList<edge>();
		width = 30;
		height = 30;
	    }

	    public interfgr(String name) { //Construct with label
		this.setTitle(name);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		nodes = new ArrayList<Node>();
		edges = new ArrayList<edge>();
		width = 30;
		height = 30;
	    }

	    class Node {
		int x, y;
		String name;
		
		
		public Node(String myName, int myX, int myY) {
		    x = myX;
		    y = myY;
		    name = myName;
		}


		@Override
		public String toString() {
			return "Node [name=" + name + "]";
		}
	    }
	    
	    class edge {
		int i,j,pds;
		@Override
		public String toString() {
			return "edge [i=" + i + ", j=" + j + ", pds=" + pds + ", c=" + c + "]";
		}
		Color c;
		public edge(int ii, int jj,Color c,int pd) {
		    i = ii;
		    j = jj;
		    this.c=c;
		    pds=pd;
		}
	    }
	    
	    public void addNode(String name, int x, int y) { 
		//add a node at pixel (x,y)
		nodes.add(new Node(name,x,y));
		this.repaint();
	    }
	    public void addNodeG(Graphe g) {
	    	for(int i =0;i<g.getnmax();i++) {
	    		if(g.v.get(i).getLat()!=0&&g.v.get(i).getLon()!=0) 
	    		{
	    	String s = g.v.get(i).getnom();
	    	int y = (int)(1000-((g.v.get(i).getLat())*100-4200));
	    	int x =(int)((g.v.get(i).getLon())*70+700);
	    	System.out.println(x+"  "+y);
	    	this.addNode(s, x, y);
	    		}
	    	
	    	}
	    }
	    public void addNodeT(Trajet t) {
	    	for(int i=0;i<t.nb+1;i++) {
	    	String s=	t.etapes.get(i).getnom();
	    	int x = (int)((t.etapes.get(i).getLat())*100-4000)*2-300;
	    	int y =(int)(1000-(t.etapes.get(i).getLon())*200+300);
	    	this.addNode(s, x, y);
	    	}
	    }
	    public void addNodeTrdis(String s,Graphe g) { // trajet disponible d'une ville 
	    	ArrayList<Trajet> t = g.villesAccTraj(s);
	    	for(int i=0;i<t.size();i++) {
	    		this.addNodeT(t.get(i));
	    	}
	    }
	    public void addEdge(int i, int j,Color c,int pd) {
		//add an edge between nodes i and j
		edges.add(new edge(i,j,c,pd));
		this.repaint();
	    }
	   public void addEdgeG(Graphe g) {
		   int f;
		   Couleur r;
		   int m=g.getnmax();
		   for(int i=0;i<m;i++) {
			   for(int j=0;j<m;j++) {
				   f=g.g.get(j).get(i);
				   r= g.c.get(j).get(i);
				   
				   if(f!=0) {
					   this.addEdge(i,j,Couleur.convert(r),f);
					   
				   }
			   }
			   
		   }
	   }
	   public void addEdgeT(Trajet t,Graphe g) {
		   int h =t.getNb();
		   System.out.println(t.etapes);
		   System.out.println(h);
		   for(int i=0;i<t.getNb();++i)
		   {for(int j=i+1;j<i+2;j++) {
			   Sommet a= t.etapes.get(i),b=t.etapes.get(i+1);
			   int x = g.dist(a.getnom(),b.getnom());
			   Color c = Couleur.convert(t.couleurs.get(i));
			   System.out.println(c+" "+x+" "+i+" "+j);
			   this.addEdge(i,j,c,x);
		   }
		   }
	   }
	   public void addEdgeTrdis(String s,Graphe g) {
			ArrayList<Trajet> t = g.villesAccTraj(s);
	    	for(int i=0;i<t.size();i++) {
	    		this.addEdgeT(t.get(i),g);
	    	}
	   }
	    
	    public void paint(Graphics g) { // draw the nodes and edges
		FontMetrics f = g.getFontMetrics();
		int nodeHeight = Math.max(height, f.getHeight());
		Graphics2D g1 = (Graphics2D) g;
		
		
		for (edge e : edges) {
			g1.setColor(e.c);
			
			g1.drawString(e.pds+"KM", (nodes.get(e.i).x+ nodes.get(e.j).x)/2+12,
					 (nodes.get(e.i).y+ nodes.get(e.j).y)/2+5);
			BasicStroke line = new BasicStroke(5.0f);
			g1.setStroke(line);
		    g1.drawLine(nodes.get(e.i).x, nodes.get(e.i).y,
			     nodes.get(e.j).x, nodes.get(e.j).y);
		    g1.setBackground(new Color(192,192,192));
		    
		}

		for (Node n : nodes) {
		    int nodeWidth = Math.max(width, f.stringWidth(n.name)+width/2);
		    g1.setColor(Color.white);
		   g1.fillRect(n.x-7, n.y-3, 
			       7, 7 );
		    g1.setColor(Color.white);
		   // g1.drawOval(n.x-nodeWidth/2, n.y-nodeHeight/2, 
			    //   nodeWidth, nodeHeight);
	        g1.setFont(new Font("impact", Font.BOLD, 15)); 

		    g1.drawString(n.name, n.x-f.stringWidth(n.name)/2 +36,
				 n.y+f.getHeight()/2);
		    g=g1;
		}
	    }
	    public static void main( ) {
	    	
	    }
	}