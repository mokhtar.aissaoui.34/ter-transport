-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : Dim 15 mars 2020 à 21:18
-- Version du serveur :  8.0.19
-- Version de PHP : 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `ter`
--

-- --------------------------------------------------------

--
-- Structure de la table `villes`
--

CREATE TABLE `villes` (
  `ville_nom_simple` varchar(45) DEFAULT NULL,
  `ville_nom_reel` varchar(45) DEFAULT NULL,
  `ville_population_2012` mediumint UNSIGNED DEFAULT NULL COMMENT 'approximatif',
  `ville_longitude_deg` float DEFAULT NULL,
  `ville_latitude_deg` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `villes`
--

INSERT INTO `villes` (`ville_nom_simple`, `ville_nom_reel`, `ville_population_2012`, `ville_longitude_deg`, `ville_latitude_deg`) VALUES
('paris', 'Paris', 2211000, 2.34445, 48.86),
('marseille', 'Marseille', 851400, 5.37639, 43.2967),
('lyon', 'Lyon', 474900, 4.84139, 45.7589),
('toulouse', 'Toulouse', 439600, 1.43333, 43.6),
('nice', 'Nice', 344900, 7.25, 43.7),
('nantes', 'Nantes', 283300, -1.55, 47.2167),
('strasbourg', 'Strasbourg', 272100, 7.75, 48.5833),
('montpellier', 'Montpellier', 253000, 3.88333, 43.6),
('bordeaux', 'Bordeaux', 235900, -0.566667, 44.8333),
('lille', 'Lille', 225800, 3.06667, 50.6333),
('rennes', 'Rennes', 206700, -1.68333, 48.0833),
('reims', 'Reims', 181500, 4.03333, 49.25),
('le havre', 'Le Havre', 178800, 0.133333, 49.5),
('saint etienne', 'Saint-Étienne', 172700, 4.4, 45.4333),
('toulon', 'Toulon', 166700, 5.93333, 43.1167),
('grenoble', 'Grenoble', 156700, 5.71667, 45.1667),
('dijon', 'Dijon', 151600, 5.01667, 47.3167),
('angers', 'Angers', 148400, -0.55, 47.4667),
('ste clotilde', 'Saint-Denis', 145022, 46.7107, 1.71819),
('le mans', 'Le Mans', 143500, 0.2, 48),
('aix en provence', 'Aix-en-Provence', 142700, 5.43333, 43.5333),
('brest', 'Brest', 142100, -4.48333, 48.4),
('villeurbanne', 'Villeurbanne', 141100, 4.88333, 45.7667),
('nimes', 'Nîmes', 140300, 4.35, 43.8333),
('limoges', 'Limoges', 140100, 1.25, 45.85),
('clermont ferrand', 'Clermont-Ferrand', 139000, 3.08333, 45.7833),
('tours', 'Tours', 135500, 0.683333, 47.3833),
('amiens', 'Amiens', 134400, 2.3, 49.9),
('metz', 'Metz', 122800, 6.16667, 49.1333),
('besancon', 'Besançon', 117600, 6.03333, 47.25),
('perpignan', 'Perpignan', 116700, 2.88333, 42.6833),
('orleans', 'Orléans', 113300, 1.9, 47.9167),
('boulogne billancourt', 'Boulogne-Billancourt', 112200, 2.25, 48.8333),
('mulhouse', 'Mulhouse', 111900, 7.33333, 47.75),
('caen', 'Caen', 109900, -0.35, 49.1833),
('rouen', 'Rouen', 109400, 1.08333, 49.4333),
('nancy', 'Nancy', 106400, 6.2, 48.6833),
('saint denis', 'Saint-Denis', 103700, 2.35833, 48.9333),
('st paul', 'Saint-Paul', 103346, -21.0033, 55.2833),
('argenteuil', 'Argenteuil', 103300, 2.25, 48.95),
('montreuil', 'Montreuil', 102200, 2.43333, 48.8667),
('roubaix', 'Roubaix', 95900, 3.16667, 50.7),
('dunkerque', 'Dunkerque', 93600, 2.36667, 51.05),
('tourcoing', 'Tourcoing', 92600, 3.15, 50.7167),
('avignon', 'Avignon', 90100, 4.81667, 43.95),
('nanterre', 'Nanterre', 89600, 2.2, 48.9),
('poitiers', 'Poitiers', 89300, 0.333333, 46.5833),
('creteil', 'Créteil', 89300, 2.46667, 48.7833),
('fort de france', 'Fort-de-France', 87216, 14.627, -61.0732),
('versailles', 'Versailles', 86700, 2.13333, 48.8),
('courbevoie', 'Courbevoie', 85100, 2.25222, 48.8973),
('vitry sur seine', 'Vitry-sur-Seine', 84100, 2.4, 48.7833),
('pau', 'Pau', 84000, -0.366667, 43.3),
('colombes', 'Colombes', 83700, 2.25, 48.9167),
('aulnay sous bois', 'Aulnay-sous-Bois', 82200, 2.51667, 48.95),
('asnieres sur seine', 'Asnières-sur-Seine', 81700, 2.28556, 48.9112),
('ravine des cabris', 'Saint-Pierre', 79228, 43.6566, 6.87387),
('rueil malmaison', 'Rueil-Malmaison', 78100, 2.2, 48.8833),
('antibes', 'Antibes', 77000, 7.11667, 43.5833),
('la rochelle', 'La Rochelle', 75800, -1.15, 46.1667),
('saint maur des fosses', 'Saint-Maur-des-Fossés', 75700, 2.5, 48.8),
('champigny sur marne', 'Champigny-sur-Marne', 75100, 2.51667, 48.8167),
('calais', 'Calais', 74800, 1.83333, 50.95),
('aubervilliers', 'Aubervilliers', 74500, 2.38333, 48.9167),
('le tampon', 'Tampon', 73365, -21.2815, 55.518),
('cannes', 'Cannes', 72900, 7.01667, 43.55),
('beziers', 'Béziers', 71700, 3.25, 43.35),
('bourges', 'Bourges', 69000, 2.4, 47.0833),
('saint nazaire', 'Saint-Nazaire', 66900, -2.2, 47.2833),
('colmar', 'Colmar', 66900, 7.36667, 48.0833),
('drancy', 'Drancy', 66200, 2.45, 48.9333),
('merignac', 'Mérignac', 66100, -0.633333, 44.8333),
('ajaccio', 'Ajaccio', 65200, 8.7364, 41.9256),
('valence', 'Valence', 64500, 4.9, 44.9333),
('quimper', 'Quimper', 63900, -4.1, 48),
('issy les moulineaux', 'Issy-les-Moulineaux', 63300, 2.26667, 48.8167),
('noisy le grand', 'Noisy-le-Grand', 63100, 2.56667, 48.85),
('levallois perret', 'Levallois-Perret', 63000, 2.28333, 48.9),
('villeneuve d ascq', 'Villeneuve-d\'Ascq', 62700, 3.14167, 50.6833),
('troyes', 'Troyes', 61500, 4.08333, 48.3),
('antony', 'Antony', 61200, 2.3, 48.75),
('neuilly sur seine', 'Neuilly-sur-Seine', 60300, 2.26667, 48.8833),
('la seyne sur mer', 'La Seyne-sur-Mer', 60000, 5.88333, 43.1),
('sarcelles', 'Sarcelles', 59200, 2.38333, 49),
('les abymes', 'Abymes', 58534, 16.3065, -61.5106),
('clichy', 'Clichy', 58400, 2.3, 48.9),
('lorient', 'Lorient', 58100, -3.36667, 47.75),
('niort', 'Niort', 58100, -0.466667, 46.3167),
('venissieux', 'Vénissieux', 57600, 4.88333, 45.6833),
('pessac', 'Pessac', 57600, -0.616667, 44.8),
('mamoudzou', 'Mamoudzou', 57281, 45.2317, -12.7814),
('saint quentin', 'Saint-Quentin', 56800, 3.28333, 49.85),
('chambery', 'Chambéry', 56800, 5.93333, 45.5667),
('ivry sur seine', 'Ivry-sur-Seine', 56700, 2.38333, 48.8167),
('cergy', 'Cergy', 56100, 2.06667, 49.0333),
('montauban', 'Montauban', 56000, 1.35, 44.0167),
('cayenne', 'Cayenne', 55753, 4.92215, -52.3054),
('hyeres', 'Hyères', 55100, 6.11667, 43.1167),
('beauvais', 'Beauvais', 55000, 2.08333, 49.4333),
('cholet', 'Cholet', 54100, -0.883333, 47.0667),
('st andre', 'Saint-André', 53955, -20.9599, 55.6539),
('bondy', 'Bondy', 53300, 2.46667, 48.9),
('villejuif', 'Villejuif', 53200, 2.36667, 48.8),
('vannes', 'Vannes', 53000, -2.75, 47.6667),
('maisons alfort', 'Maisons-Alfort', 52900, 2.43333, 48.8),
('fontenay sous bois', 'Fontenay-sous-Bois', 52800, 2.48333, 48.85),
('chelles', 'Chelles', 52800, 2.6, 48.8833),
('frejus', 'Fréjus', 52700, 6.73611, 43.4339),
('pantin', 'Pantin', 52700, 2.4, 48.9),
('epinay sur seine', 'Épinay-sur-Seine', 52700, 2.30833, 48.95),
('arles', 'Arles', 52700, 4.63333, 43.6667),
('evry', 'Évry', 52500, 2.45, 48.6333),
('st louis', 'Saint-Louis', 52038, 0, 0),
('la roche sur yon', 'La Roche-sur-Yon', 51700, -1.43333, 46.6667),
('grasse', 'Grasse', 51600, 6.91667, 43.6667),
('sartrouville', 'Sartrouville', 51400, 2.18333, 48.95),
('clamart', 'Clamart', 51400, 2.26667, 48.8),
('narbonne', 'Narbonne', 51000, 3, 43.1833),
('charleville mezieres', 'Charleville-Mézières', 50900, 4.71667, 49.7667),
('laval', 'Laval', 50900, -0.766667, 48.0667),
('sevran', 'Sevran', 50800, 2.53333, 48.9333),
('evreux', 'Évreux', 50800, 1.15, 49.0167),
('le blanc mesnil', 'Le Blanc-Mesnil', 50700, 2.45, 48.9333),
('belfort', 'Belfort', 50300, 6.86667, 47.6333),
('annecy', 'Annecy', 50100, 6.11667, 45.9),
('brive la gaillarde', 'Brive-la-Gaillarde', 49700, 1.53333, 45.15),
('cagnes sur mer', 'Cagnes-sur-Mer', 48900, 7.15, 43.6667),
('albi', 'Albi', 48800, 2.15, 43.9333),
('meaux', 'Meaux', 48700, 2.86667, 48.95),
('saint malo', 'Saint-Malo', 48200, -2.01667, 48.65),
('vincennes', 'Vincennes', 48100, 2.43333, 48.85),
('bobigny', 'Bobigny', 47700, 2.45, 48.9),
('carcassonne', 'Carcassonne', 47600, 2.35, 43.2167),
('blois', 'Blois', 46800, 1.33333, 47.5833),
('montrouge', 'Montrouge', 46700, 2.31667, 48.8167),
('martigues', 'Martigues', 46500, 5.05, 43.4),
('aubagne', 'Aubagne', 46100, 5.56667, 43.2833),
('chalons en champagne', 'Châlons-en-Champagne', 46100, 4.36667, 48.9583),
('chalon sur saone', 'Chalon-sur-Saône', 46000, 4.85, 46.7833),
('chateauroux', 'Châteauroux', 46000, 1.7, 46.8167),
('saint brieuc', 'Saint-Brieuc', 45900, -2.78333, 48.5167),
('suresnes', 'Suresnes', 45600, 2.23333, 48.8667),
('saint ouen', 'Saint-Ouen', 45600, 2.33333, 48.9),
('meudon', 'Meudon', 44700, 2.24167, 48.8083),
('alfortville', 'Alfortville', 44700, 2.41667, 48.8),
('puteaux', 'Puteaux', 44500, 2.23333, 48.8667),
('bayonne', 'Bayonne', 44500, -1.48333, 43.4833),
('tarbes', 'Tarbes', 44200, 0.083333, 43.2333),
('boulogne sur mer', 'Boulogne-sur-Mer', 43800, 1.61667, 50.7167),
('bastia', 'Bastia', 43500, 9.44945, 42.7),
('saint herblain', 'Saint-Herblain', 43200, -1.65, 47.2167),
('angouleme', 'Angoulême', 43100, 0.15, 45.65),
('castres', 'Castres', 43000, 2.25, 43.6),
('sete', 'Sète', 42800, 3.68333, 43.4),
('arras', 'Arras', 42800, 2.78333, 50.2833),
('valenciennes', 'Valenciennes', 42700, 3.53333, 50.35),
('mantes la jolie', 'Mantes-la-Jolie', 42600, 1.71667, 48.9833),
('istres', 'Istres', 42600, 4.98333, 43.5167),
('douai', 'Douai', 42400, 3.06667, 50.3667),
('wattrelos', 'Wattrelos', 41800, 3.21667, 50.7),
('gennevilliers', 'Gennevilliers', 41800, 2.3, 48.9333),
('livry gargan', 'Livry-Gargan', 41800, 2.55, 48.9167),
('corbeil essonnes', 'Corbeil-Essonnes', 41700, 2.48333, 48.6),
('compiegne', 'Compiègne', 41600, 2.83333, 49.4167),
('saint priest', 'Saint-Priest', 41500, 4.93333, 45.6917),
('salon de provence', 'Salon-de-Provence', 41400, 5.1, 43.6333),
('thionville', 'Thionville', 41100, 6.16667, 49.3667),
('caluire et cuire', 'Caluire-et-Cuire', 40900, 4.85, 45.8),
('saint germain en laye', 'Saint-Germain-en-Laye', 40900, 2.08333, 48.9),
('le cannet', 'Le Cannet', 40900, 7.01667, 43.5667),
('rosny sous bois', 'Rosny-sous-Bois', 40800, 2.48333, 48.8667),
('talence', 'Talence', 40600, -0.6, 44.8167),
('ales', 'Alès', 40500, 4.08333, 44.1333),
('massy', 'Massy', 40500, 2.28333, 48.7333),
('vaulx en velin', 'Vaulx-en-Velin', 40400, 4.93333, 45.7833),
('bourg en bresse', 'Bourg-en-Bresse', 40200, 5.21667, 46.2),
('cherbourg octeville', 'Cherbourg-Octeville', 39800, -1.65, 49.65),
('garges les gonesse', 'Garges-lès-Gonesse', 39600, 2.41667, 48.9667),
('montlucon', 'Montluçon', 39500, 2.6, 46.3333),
('le lamentin', 'Lamentin', 39360, 14.6191, -60.9946),
('chartres', 'Chartres', 39200, 1.5, 48.45),
('bron', 'Bron', 39100, 4.91667, 45.7333),
('marcq en baroeul', 'Marcq-en-Barœul', 38900, 3.08333, 50.6667),
('melun', 'Melun', 38900, 2.66667, 48.5333),
('noisy le sec', 'Noisy-le-Sec', 38700, 2.46667, 48.8833),
('gap', 'Gap', 38600, 6.08333, 44.5667),
('bagneux', 'Bagneux', 38500, 2.3, 48.8),
('le port', 'Port', 38418, -20.9348, 55.2987),
('st laurent du maroni', 'Saint-Laurent-du-Maroni', 38367, 5.28697, -53.7897),
('gagny', 'Gagny', 38300, 2.53333, 48.8833),
('choisy le roi', 'Choisy-le-Roi', 38200, 2.41667, 48.7667),
('reze', 'Rezé', 37900, -1.56667, 47.2),
('anglet', 'Anglet', 37900, -1.53333, 43.4833),
('nevers', 'Nevers', 37600, 3.16667, 46.9833),
('poissy', 'Poissy', 37500, 2.05, 48.9333),
('savigny sur orge', 'Savigny-sur-Orge', 37300, 2.35, 48.6667),
('st martin', 'Saint-Martin', 36979, 18.0913, -63.0829),
('auxerre', 'Auxerre', 36900, 3.56667, 47.8),
('la courneuve', 'La Courneuve', 36900, 2.38333, 48.9167),
('draguignan', 'Draguignan', 36600, 6.46667, 43.5333),
('aubenas', 'Aubenas', 1000, 4.38, 44.62),
('lunel', 'Lunel', 0, 4.13, 43.67),
('aubenas', 'Aubenas', 1000, 4.38, 44.62),
('lunel', 'Lunel', 0, 4.13, 43.67);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `villes`
--
ALTER TABLE `villes`
  ADD KEY `ville_nom_reel` (`ville_nom_reel`),
  ADD KEY `ville_longitude_latitude_deg` (`ville_longitude_deg`,`ville_latitude_deg`),
  ADD KEY `ville_nom_simple` (`ville_nom_simple`),
  ADD KEY `ville_longitude_deg` (`ville_longitude_deg`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
